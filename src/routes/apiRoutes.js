const router = require('express').Router()
const userController = require ('../controller/userController');
const salaDeAulaController = require ('../controller/salaDeAulaController');
const dbController = require ('../controller/dbController');

//ALUNO
router.post('/cadastroUsuario/',userController.loginUser)
router.post('/loginUsuario/',userController.loginUser)
router.put('/updateUsuario/', userController.updateUser)
router.delete('/deleteUsuario/', userController.deleteUser)
router.get('/getUsuario/', userController.getUser)

//SALA DE AULA
router.post('/cadastroSalaDeAula/',salaDeAulaController.cadastroSalaDeAula)
router.put('/updateSalaDeAula/', salaDeAulaController.updateSalaDeAula)
router.delete('/deleteSalaDeAula/', salaDeAulaController.deleteSalaDeAula)
router.get('/getSalaDeAula/',salaDeAulaController.getSalaDeAula)

//API COM BANCO DE DADOS,ROTA PARA CONSULTAR A TABELAS DO BANCO
router.get('/tables/',dbController.getNameTables)
router.get('/tablesdescriptions/', dbController.getTablesDescription); //rota para consulta das descrições da tabela do banco 

module.exports = router;