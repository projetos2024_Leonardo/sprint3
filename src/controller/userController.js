module.exports = class alunoController {
//Método para realizar login do usuário
  static async loginUser(req, res) {
    const { nome, senha } = req.body;
  //Verifica se os campos estão preenchidos
    if (nome !== "" && senha !== "") {
      return res.status(200).json({ message: "Logado com sucesso!" });
    } else {
      return res.status(500).json({ message: "Preencha os dados corretamente!" });
    }//Fim do else
    }//Fim do loginUser

//Método para realizar cadastro do usuário
  static async cadastroUser(req, res) {
    const { nome, email, senha } = req.body;
 //Verifica se os campos estao preenchidos
    if (nome === "" || email === "" || senha === "") {
      return res.status(500).json({ message: "Preencha todos os campos corretamente!" });
    }//Fim do if campos nulos
  //Verifica se o email contem @
    if (!email.includes("@")) {
      return res.status(500).json({ message: "O email fornecido não é válido!" });
    }//Fim do if verificação do email
  // Verifica se a senha tem pelo menos 8 caracteres e contém letras maiúsculas e minúsculas
    if (!(senha.length >= 8 && /[a-z]/.test(senha) && /[A-Z]/.test(senha))) {
  return res.status(500).json({message: "A senha deve conter pelo menos 8 caracteres e incluir letra maiúscula e letra minúscula!",});
  }//Fim do if verificação de senha
    return res.status(200).json({ message: "Cadastrado com sucesso!" });
  }//Fim do cadastroUser

//Método para realizar atualização do usuário
  static async updateUser(req, res) {
    const { nome, email, senha } = req.body;
  //Verifica se os campos estao preenchidos
    if (nome === "" || email === "" || senha === "") {
      return res.status(500).json({ message: "Preencha todos os campos corretamente!" });
    }//Fim do if verificação campos nulos 
    //Verifica se o email contem @
    if (!email.includes("@")) {
      return res.status(500).json({ message: "O email fornecido não é válido!" });
    }//Fim do if verificação do email
  // Verifica se a senha tem pelo menos 8 caracteres e contém letras maiúsculas e minúsculas
    if (!(senha.length >= 8 && /[a-z]/.test(senha) && /[A-Z]/.test(senha))) {
      return res.status(500).json({message: "A senha deve conter pelo menos 8 caracteres e incluir letra maiúscula e letra minúscula!",});
    }//Fim do if verificação de senha
    return res.status(200).json({ message: "Atualizado com sucesso!" });
  }//Fim do UpdateUser

//Método para solicitar dados 
  static async getUser(req, res) {
    const nome = "Victor";
    const email = "Victor@gmail";
    const senha = "123";
    return res.status(200).json({ nome: nome, email: email, senha: senha });
  }//Fim do get 

//Método para deletar usuário
  static async deleteUser(req, res) {
    try {
        const userId = req.params.id;
        return res.status(200).json({ message: "usuário removido com sucesso!", userId: userId });
    } catch (error) {
        console.error("Erro ao excluir usuário!", error);
        res.status(500).json({ message: "Ocorreu um erro ao tentar excluir!" });
    }//Fim do catch
} //Fim deleteUser
}; //Fim da class


//testar git 21/05
